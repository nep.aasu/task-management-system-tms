import express, { json } from "express";
import { port } from "./src/config.js";
import cors from "cors";
import connectToMongoDB from "./src/database/connectToDB.js";
import { config } from "dotenv";
import firstRouter from "./src/router/firstRouter.js";
import taskRouter from "./src/router/taskRouter.js";
import userRouter from "./src/router/userRouter.js";

const expressApp = express();
expressApp.use(json());
expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});

connectToMongoDB();
config();
expressApp.use(cors());

expressApp.use("/", firstRouter);
expressApp.use("/users", userRouter);
expressApp.use("/tasks", taskRouter);
