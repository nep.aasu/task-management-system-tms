import { Task, User } from "../schema/model.js";

export let createTask = async (req, res, next) => {
  let _id = req._id;

  try {
    let data = req.body;
    let createdByUser = await User.findById(_id);

    let assignedToUser = await User.findById(data.assignedTo);

    data = {
      ...data,
      createdBy: createdByUser.fullName,
      createdByEmail: createdByUser.email,
      assignedTo: assignedToUser.fullName,
      assignedToEmail: assignedToUser.email,
    };
    let result = await Task.create(data);
    res.status(200).json({
      success: true,
      message: "task created",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readTask = async (req, res) => {
  let _id = req._id;

  try {
    let user = await User.findById(_id);

    let userEmail = user.email;

    let result = await Task.find({
      $or: [{ createdByEmail: userEmail }, { assignedToEmail: userEmail }],
    });

    res.json({
      success: true,
      message: "task get by login successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readTaskDetails = async (req, res) => {
  let taskId = req.params.taskId;
  try {
    let result = await Task.findById(taskId);
    res.json({
      success: true,
      message: "find by id",
      result: result,
    });
  } catch (error) {
    res.json({
      success: true,
      message: error.message,
    });
  }
};

export let updateTask = async (req, res) => {
  let taskId = req.params.taskId;
  let data = req.body;
  try {
    let result = await Task.findByIdAndUpdate(taskId, data);
    res.json({
      success: true,
      message: "updated",
      result: result,
    });
  } catch (error) {
    res.json({
      success: true,
      message: error.message,
    });
  }
};

export let deleteTask = async (req, res) => {
  let taskId = req.params.taskId;
  try {
    let result = await Task.findByIdAndDelete(taskId);
    res.json({
      success: true,
      message: "delete",
      result: result,
    });
  } catch (error) {
    res.json({
      success: true,
      message: error.message,
    });
  }
};
