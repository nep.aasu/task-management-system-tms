import { Router } from "express";
import {
  createTask,
  deleteTask,
  readTask,
  readTaskDetails,
  updateTask,
} from "../controller/taskController.js";
import { isAuthenticated } from "../middleware/isAuthenticated.js";
import authorized from "../middleware/authorized.js";
import isAuthorized from "../middleware/isAuthorized.js";

let taskRouter = Router();

taskRouter
  .route("/")
  .post(
    isAuthenticated,
    authorized(["admin", "superAdmin", "user"]),
    createTask
  )
  .get(isAuthenticated, readTask);

taskRouter
  .route("/:taskId")
  .get(isAuthenticated, readTaskDetails)
  .patch(isAuthenticated, updateTask)
  .delete(isAuthenticated, authorized(["admin", "superAdmin"]), deleteTask);

export default taskRouter;
