import { Router } from "express";

import {
  createUser,
  deleteSpecificUser,
  forgetPassword,
  myProfile,
  readAllUser,
  readSpecificUser,
  resetPassword,
  updatePassword,
  updateProfile,
  updateSpecificUser,
  userLogin,
  verifyEmail,
  verifyOtp,
} from "../controller/userController.js";
import { isAuthenticated } from "../middleware/isAuthenticated.js";
import authorized from "../middleware/authorized.js";
import { validation } from "../middleware/validation.js";
import { userValidation } from "../validation/userValidation.js";

let userRouter = Router();

userRouter
  .route("/")
  .post(validation(userValidation), createUser)
  .get(
    isAuthenticated,
    authorized(["admin", "superAdmin", "user"]),
    readAllUser
  );

userRouter.route("/verify-email").post(verifyEmail);

userRouter.route("/login").post(userLogin);

userRouter.route("/my-profile").get(isAuthenticated, myProfile);

userRouter.route("/update-profile").patch(isAuthenticated, updateProfile);

userRouter.route("/update-password").patch(isAuthenticated, updatePassword);

userRouter.route("/forget-password").post(forgetPassword);

userRouter.route("/verify-otp").post(isAuthenticated, verifyOtp);

userRouter.route("/reset-password").patch(isAuthenticated, resetPassword);

userRouter
  .route("/:id")
  .get(isAuthenticated, authorized(["admin", "superAdmin"]), readSpecificUser)
  .patch(
    isAuthenticated,
    authorized(["admin", "superAdmin"]),
    updateSpecificUser
  )
  .delete(isAuthenticated, authorized(["superAdmin"]), deleteSpecificUser);

export default userRouter;
