import { Router } from "express";

let firstRouter = Router();

firstRouter
  .route("/")

  .post((req, res) => {
    console.log(`body send data: `, req.body);
    console.log(`query send data: `, req.query);
    console.log(`params send data: `, req.params);
    res.json("home post");
  });

firstRouter.route("/name").post((req, res) => {
  res.json("name post");
});

firstRouter.route("/a/:country/b/:name").post((req, res) => {
  console.log("params", req.params);
  res.json(":a response hello");
});
export default firstRouter;
