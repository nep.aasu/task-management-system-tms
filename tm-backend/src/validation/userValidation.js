import Joi from "joi";

export const userValidation = Joi.object()
  .keys({
    fullName: Joi.string().required().min(3),
    email: Joi.string()
      .required()
      .custom((value, msg) => {
        let isValidEmail = value.match(
          /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
        );
        if (isValidEmail) {
          return true;
        } else {
          return msg.message(
            "Invalid email address. Please enter a valid email."
          );
        }
      }),
    password: Joi.string()
      .required()
      .custom((value, msg) => {
        let isValidPassword = value.match(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
        );

        if (isValidPassword) {
          return true;
        } else {
          return msg.message(
            "password must contain At least 8 characters, one uppercase, one lowercase letter, digit (number) and one special character"
          );
        }
      }),
    dob: Joi.date()
      .required()
      .custom((value, msg) => {
        const enteredDate = new Date(value);
        const currentDate = new Date();
        const age = currentDate.getFullYear() - enteredDate.getFullYear();
        if (age >= 18) {
          return true;
        } else {
          msg.message("You must be at least 18 years old.");
        }
      }),
    gender: Joi.string().required().valid("male", "female", "other"),
    role: Joi.string().required().valid("superAdmin", "admin", "user"),
  })
  .unknown(true);
