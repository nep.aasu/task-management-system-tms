import { Schema } from "mongoose";

const userSchema = Schema(
  {
    fullName: {
      type: String,
      required: [true, "fullName is required"],
    },

    email: {
      type: String,
      unique: true,
      required: [true, "email is required"],
    },
    password: {
      type: String,
      required: [true, "password is required"],
    },
    dob: {
      type: Date,
      required: [true, "dob is required"],
    },
    gender: {
      type: String,
      required: [true, "gender is required"],
    },
    role: {
      type: String,
      required: [true, "role is required"],
    },
  },
  { timestamps: true }
);

export default userSchema;
