import { Schema } from "mongoose";

let taskSchema = Schema(
  {
    title: {
      type: String,
      required: [true, "name is required."],
    },
    description: {
      type: String,
      required: [true, "age is required."],
    },
    dueDate: {
      type: Date,
      required: [true, "isMarried is required."],
    },
    priority: {
      type: String,
      required: [true, "isMarried is required."],
    },
    status: {
      type: String,
      required: [true, "isMarried is required."],
    },
    assignedTo: {
      type: String,
      required: [true, "isMarried is required."],
    },
    assignedToEmail: {
      type: String,
      required: [true, "isMarried is required."],
    },
    createdBy: {
      type: String,
      required: [true, "isMarried is required."],
    },
    createdByEmail: {
      type: String,
      required: [true, "isMarried is required."],
    },
  },
  { timestamps: true }
);

export default taskSchema;
