import { config } from "dotenv";

config();
export let port = process.env.PORT;
export const dbUrl = process.env.DB_URL;
export const email = process.env.EMAIL;
export const password = process.env.PASSWORD;
export const secretKey = process.env.SECRET_KEY;
