import mongoose from "mongoose";
import { dbUrl } from "../config.js";

let connectToMongoDB = async () => {
  try {
    mongoose.connect(`${dbUrl}`);
    console.log("connected to DB");
  } catch (error) {
    console.log(error.message);
  }
};
export default connectToMongoDB;
