import { Task, User } from "../schema/model.js";

let isAuthorized = async (req, res, next) => {
  try {
    const userId = req._id;
    const userName = await Task.filter((value, i) => {
      if (value === userId.assignedTo) {
        return console.log(userId.assignedTo);
      }
    });
  } catch (error) {
    console.error("Error in isAuthorized middleware:", error);
    res.status(500).json({
      success: false,
      message: "Internal server error",
    });
  }
};

export default isAuthorized;
